<?php

require_once('vendor/autoload.php');

if (count($argv) < 2) {
    print_r("Missing file argument\n");
    return 1;
}

if (count($argv) > 2) {
    print_r("Too much arguments\n");
    return 2;
}

try {
    $mp = MiniPng\Utility\Create::fromFile($argv[1]);
} catch (Exception $e) {
    print_r("An error was thrown:\n${e}\n");
    return 3;
}

$pxTypes = [
    0 => 'noir et blanc',
    1 => 'nuances de gris',
    2 => 'palette de couleurs',
    3 => 'matrice de couleurs',
];

$expr = 'Largeur : ' . $mp->getHeader()['length'] . "\n"
    . 'Hauteur : ' . $mp->getHeader()['height'] . "\n"
    . 'Type de pixel : ' . $mp->getHeader()['pixelType']
    . ' (' . $pxTypes[$mp->getHeader()['pixelType']] . ')' . "\n"
    . "Commentaires : \n" . $mp->getComment() . "\n";

print_r($expr);
return $expr;
