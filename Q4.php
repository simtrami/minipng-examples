<?php

use MiniPng\Type\BW;

require_once('vendor/autoload.php');

if (count($argv) < 2) {
    print_r("Missing file argument\n");
    return 1;
}

if (count($argv) > 2) {
    print_r("Too much arguments\n");
    return 2;
}

try {
    $mp = MiniPng\Utility\Create::fromFile($argv[1]);
} catch (Exception $e) {
    print_r("An error was thrown:\n${e}\n");
    return 3;
}

if (! $mp instanceof BW) {
    print_r("The given file cannot be instantiated to a BW MiniPNG\nUse Q9 instead to convert it into a known format.\n");
    return 4;
}

$bmp = $mp->getBitmap();

$expr = '';
foreach ($bmp as $ln) {
    foreach ($ln as $b) {
        $expr .= $b === '0' ? 'X' : ' ';
    }
    $expr .= "\n";
}

print_r($expr);
return $expr;
