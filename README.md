# minipng-examples

This project shows how to use the
[simtrami/minipng](https://gitlab.com/simtrami/minipng) library.

## Installation

```shell script
git clone https://gitlab.com/simtrami/minipng-examples.git
cd minipng-examples
composer install
```

## Usage

### Q1

Displays the header's content

```shell script
php Q1.php 'path/to/file.mp'
```

### Q2

Displays the header's content and the comment (can be empty)

```shell script
php Q2.php 'path/to/file.mp'
```

### Q4

Displays an ASCII representation of a B&W .mp file

```shell script
php Q4.php 'path/to/bw/file.mp'
```

### Q9

Converts the given .mp file into a readable Netbpm (Portable pixmap) file

```shell script
php Q4.php 'path/to/file.mp' 'path/to/target/file.pnm'
```
