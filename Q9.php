<?php

require_once('vendor/autoload.php');

if (count($argv) < 2) {
    print_r("Missing file argument\n");
    return 1;
}

if (count($argv) < 3) {
    print_r("Missing target argument\n");
    return 5;
}

if (count($argv) > 3) {
    print_r("Too much arguments\n");
    return 2;
}

try {
    $mp = new MiniPng\Utility\Convert();
    $mp->to_PNM($argv[1], $argv[2]);
} catch (Exception $e) {
    print_r("An error was thrown:\n${e}\n");
    return 4;
}

$expr = "Successfully converted file\n";
print_r($expr);
return $expr;
